package co.williamhill.tdd.dag;

import java.util.Collection;

public class NodeImpl implements Node {

    static final String CAN_NOT_ADD_CHILD_MESSAGE = "You can not add this as a child, this node is already a parent";
    static final String CAN_NOT_ADD_PARENT_MESSAGE = "You can not add this as a parent, this node is already a child";
    static final String CAN_NOT_ADD_ITSELF = "Node itself can not be a parent or child";

    private String name;

    public NodeImpl(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void addChild(Node child) {

    }

    @Override
    public void addParent(Node parent) {

    }

    @Override
    public Collection<Node> getChildren() {
        return null;
    }

    @Override
    public Collection<Node> getParents() {
        return null;
    }
}
