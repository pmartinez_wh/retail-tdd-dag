package co.williamhill.tdd.dag;

import java.util.Collection;

public interface Node {

    String getName();
    void addChild(Node child);
    void addParent(Node parent);
    Collection<Node> getChildren();
    Collection<Node> getParents();
}
